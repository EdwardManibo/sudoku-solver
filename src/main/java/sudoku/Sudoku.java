/*
 * Copyright (c) 2017 Edward Manibo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * EDWARD MANIBO BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Reference: https://opensource.org/licenses/MIT
 * [Accessed on 26th August 2017]
 */

package org.bitbucket.sudoku;

/**
 * A Sudoku Game
 */
public class Sudoku {
    /**
     * Build a Sudoku from a grid.
     *
     * @param g The grid that defines the sudoku
     */
    public Sudoku(int[][] g) {
        theGrid = g;
    }


    /**
     * Secondary constructor for Sudoku
     *
     * @param g The grid that defines the sudoku
     * @param e The value that denotes an empty cell
     */
    public Sudoku(int[][] g, int e) {
        theGrid = g;
        emptyValue = e;
    }

    /**
     * The n x m grid that defines the Sudoku.
     */
    private int[][] theGrid;

    /**
     * The value that denotes an empty cell.
     * Initialised with a random non-zero value to prevent the failing of Sprint 1 tests
     */
    private int emptyValue = -17;


    /**
     * Check validity of a Sudoku grid.
     *
     * @return True if and only if the grid is a valid Sudoku
     */
    public boolean isValid() {
        // Check size of grid
        if (theGrid.length == 0)
            return true;

            // Grid is not empty
            // Grid is a square
        else if (theGrid.length == theGrid[0].length) {

            /* Used this site to determine if grid length is a square number
            https://stackoverflow.com/questions/34056609/how-to-check-if-an-integer-is-a-perfect-square
            [Accessed on 25th August 2017]
            */
            double sqrt = Math.sqrt(theGrid.length);
            int sqrtInt = (int) sqrt;

            double gridDouble = Math.pow(sqrt, 2);
            double gridInt = Math.pow(sqrtInt, 2);

            // This condition will be true if and only if the grid length is a square number
            if (gridDouble == gridInt) {
                /* Used this site as a reference
                https://stackoverflow.com/questions/21922354/check-duplicate-in-rows-and-columns-2d-array
                [Accessed on 22nd August 2017]
                */
                for (int row = 0; row < theGrid.length; row++) {
                    for (int col = 0; col < theGrid[row].length; col++) {

                        // Save the current value in num variable to compare to other values
                        int num = theGrid[row][col];

                        // Ensure values in a Sudoku are valid
                        if ((num < 1 || num > theGrid.length) && num != emptyValue)
                            return false;


                        // One Rule Checks
                        // Checks whether a value repeats within the same column
                        for (int otherCol = col + 1; otherCol < theGrid.length; otherCol++) {
                            if (num != emptyValue && num == theGrid[row][otherCol])
                                return false;
                        }

                        // Checks whether a value repeats within the same row
                        for (int otherRow = row + 1; otherRow < theGrid.length; otherRow++) {
                            if (num != emptyValue && num == theGrid[otherRow][col])
                                return false;
                        }
                    }
                }

                /* Used this site as a reference
                https://stackoverflow.com/questions/34076389/java-sudoku-solution-verifier
                [Accessed on 2nd September 2017]
                */
                // Checks if the sqrt(n) x sqrt(n) square blocks
                // within Sudoku grid of size n satisfy the One rule
                for (int row = 0; row < theGrid.length; row += sqrt) {
                    for (int col = 0; col < theGrid[row].length; col += sqrt) {

                        // row, col is start of the sqrt(n) x sqrt(n) grid
                        for (int pos = 0; pos < theGrid.length - 1; pos++) {
                            for (int pos2 = pos + 1; pos2 < theGrid[row].length; pos2++) {

                                int cell1 = theGrid[row + (pos % sqrtInt)][col + (pos / sqrtInt)];
                                int cell2 = theGrid[row + (pos2 % sqrtInt)][col + (pos2 / sqrtInt)];

                                if ((cell1 != emptyValue) && (cell2 != emptyValue) && (cell1 == cell2))
                                    return false;
                            }
                        }
                    }
                }
                // If the input grid passes all of the above,
                // it is a valid Sudoku grid and so returns true
                return true;
            }
        }
        // Grid is not a n x n square, so returns false
        return false;
    }


    /**
     * Attempt to compute a solution to the Sudoku
     *
     * @return A grid with possibly less empty cells than in theGrid (but not more)
     * @note If there is no empty cell in the result, then the Sudoku is solved,
     * otherwise it is not
     */
    public int[][] solve() {
        /* Used this as a reference to solve a Sudoku
        https://www.youtube.com/watch?v=gN8xrMwrLSc
        [Accessed on 3rd September 2017]
        */

        // Input sudoku to be solved
        Sudoku g = new Sudoku(theGrid, emptyValue);

        // Only attempt to solve the Sudoku if it is valid
        if (g.isValid()) {

            // Secondary array used to keep track of the status of cells in the Sudoku
            int[][] status = new int[g.theGrid.length][g.theGrid[0].length];

            // Sets the status; for each non-empty cell in the input Sudoku,
            // status should be a fixed value (in this case, 2)
            for (int i = 0; i < g.theGrid.length; i++) {
                for (int j = 0; j < g.theGrid[0].length; j++) {
                    status[i][j] = g.theGrid[i][j] != g.emptyValue ? 2 : 0;
                }
            }
            // Attempts to solve the Sudoku, starting from the first cell
            solveSudoku(g.theGrid, status, 0, 0);
        }

        // If the input sudoku is not valid, then the original sudoku is returned
        return g.theGrid;
    }


    /**
     * Recursive helper method to solve input Sudoku
     *
     * @param board  The input sudoku
     * @param status Support array to keep status of each cell
     * @param x      X coordinate of position of interest
     * @param y      Y coordinate of position of interest
     * @return True if the input Sudoku is solved
     */
    public boolean solveSudoku(int[][] board, int[][] status, int x, int y) {
        double sqrt = Math.sqrt(board.length);
        int sqrtInt = (int) sqrt;

        if (x == board.length) {
            // Checks if all values in the Sudoku are set
            int count = 0;
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[0].length; j++) {
                    // Increments the count by 1 if the status of the current cell is non-zero
                    // (i.e., the cell has been assigned a value)
                    count += status[i][j] > 0 ? 1 : 0;
                }
            }
            // Count should be equal to the number of cells in the sudoku
            return (count == (board.length * board[0].length));
        }

        // If the current cell already has a value
        if (status[x][y] >= 1) {
            // Views the next cell
            int nextX = x;
            int nextY = y + 1;
            if (nextY == board.length) {
                nextX = x + 1;
                nextY = 0;
            }
            // Recursive call on the next position
            return solveSudoku(board, status, nextX, nextY);
        } else {
            // Creates an array to determine what values are left to input
            boolean[] used = new boolean[board.length];

            // Checks the rows
            for (int i = 0; i < board.length; i++) {
                if (status[x][i] >= 1)
                    used[board[x][i] - 1] = true;
            }

            // Checks the columns
            for (int i = 0; i < board.length; i++) {
                if (status[i][y] >= 1)
                    used[board[i][y] - 1] = true;
            }

            // Checks the sqrt(n) * sqrt(n) boxes within the Sudoku of size n
            for (int i = x - (x % sqrtInt); i < x - (x % sqrtInt) + sqrtInt; i++) {
                for (int j = y - (y % sqrtInt); j < y - (y % sqrtInt) + sqrtInt; j++) {
                    if (status[i][j] >= 1)
                        used[board[i][j] - 1] = true;
                }
            }

            for (int i = 0; i < used.length; i++) {
                // Only values that have not been used within the row/column/box can be used here
                if (!used[i]) {
                    status[x][y] = 1;
                    board[x][y] = i + 1; // Index i + 1 is the value to be inserted

                    // Repeat the index increasing operation from above
                    int nextX = x;
                    int nextY = y + 1;
                    if (nextY == board.length) {
                        nextX = x + 1;
                        nextY = 0;
                    }
                    if (solveSudoku(board, status, nextX, nextY))
                        return true;

                    // If allocating the value to an empty cell fails, reverse the status of the current cell
                    // in order to view it again and try to allocate a different value
                    for (int m = 0; m < board.length; m++) {
                        for (int n = 0; n < board[0].length; n++) {
                            // Reverse-set the values behind the current (x,y) position
                            if (m > x || (m == x && n == y)) {
                                if (status[m][n] == 1) {
                                    status[m][n] = 0;
                                    board[m][n] = 0;
                                }
                            }
                        }
                    }
                }
            }
        }
        // Default return statement to avoid compilation error
        return false;
    }

    /**
     * Attempt to efficiently compute a solution to the Sudoku
     *
     * @return A grid with possibly less empty cells than in theGrid (but not more)
     * @note If there is no empty cell in the result, then the Sudoku is solved,
     * otherwise it is not
     */
    public int[][] fastSolve() {
        // Re-use of sprint-2 solution

        // Input sudoku to be solved
        Sudoku g = new Sudoku(theGrid, emptyValue);

        // Only attempt to solve the Sudoku if it is valid
        if (g.isValid()) {

            // Secondary array used to keep track of the status of cells in the Sudoku
            int[][] status = new int[g.theGrid.length][g.theGrid[0].length];

            // Sets the status; for each non-empty cell in the input Sudoku,
            // status should be a fixed value (in this case, 2)
            for (int i = 0; i < g.theGrid.length; i++) {
                for (int j = 0; j < g.theGrid[0].length; j++) {
                    status[i][j] = g.theGrid[i][j] != g.emptyValue ? 2 : 0;
                }
            }
            // Attempts to solve the Sudoku, starting from the first cell
            solveSudoku(g.theGrid, status, 0, 0);
        }

        // If the input sudoku is not valid, then the original sudoku is returned
        return g.theGrid;
    }

}




