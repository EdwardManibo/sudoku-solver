package org.bitbucket.sudoku;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


/**
 * Test suite for checking validity of some Sudoku grids
 */
@RunWith( Parameterized.class )
public class ValidSudokuParameterisedTests {

  /**
   *  The collection of tests as an Array of (grid x expected result)
   */
  /* *INDENT-OFF* */
  @Parameters
  public static Collection<Object[]> data() {
    // testcases wil de displayed as test[0], test[1] and so on
    return Arrays.asList(new Object[][] {
      { new int[][] { }                         , true    },//0
      { new int[][] { { } }                     , false   },//1
      { new int[][] { { 1 } }                   , true    },//2
      { new int[][] { { 1 , 2 }, { 2 , 1 } }    , false   },//3
      { new int[][] { { 1 , 2 }, { 1 , 1 } }    , false   },//4
      { new int[][] { { 1 }, { 1 , 2 } }        , false   },//5

      /* Ethan Hillas tests <ethan.hillas@students.mq.edu.au> */
      {  new int[][] {
                  {1, 2},
                  {1, 2}
          }                                     , false   },//6
      { new int[][] {
              {1, 4, 2, 3, 5, 6, 7, 8, 9},
              {2, 3, 4, 1, 5, 6, 7, 8, 9},
              {4, 1, 3, 2, 5, 6, 7, 8, 9},
              {3, 2, 1, 4, 5, 6, 7, 8, 9},
              {1, 4, 2, 3, 5, 6, 7, 8, 9},
              {2, 3, 4, 1, 5, 6, 7, 8, 9},
              {4, 1, 3, 2, 5, 6, 7, 8, 9},
              {3, 2, 1, 4, 5, 6, 7, 8, 9},
              {3, 2, 1, 4, 5, 6, 7, 8, 9}
          }                                     ,  false  },//7
       { new int[][] {
               {1, 5, 2, 3},
               {2, 3, 4, 1},
               {4, 1, 3, 2},
               {3, 2, 1, 4}
           }                                    , false   },//8
       { new int[][] {
               {1, 4, 2, 3},
               {2, 3, 4, 1},
               {4, 0, 3, 2},
               {3, 2, 1, 4}
           }                                    , false   },//9 FAIL
       { new int[][] {
               {1, 4, 2, 3},
               {2, 3, 4, 1},
               {4, 1, 4, 2},
               {3, 2, 1, 4}
           }                                    , false   },//10
       { new int[][] {
               {1, 4, 2, 3},
               {2, 3, 4, 1},
               {4, 1, 3, 2},
               {3, 2, 1, 4}
           }                                    , true   },//11
       { new int[][] {
               {1, 5, 2, 3},
               {2, 3, 4, 1},
               {4, 1, 3, 2},
               {3, 2, 1, 4}
           }                                    , false  },//12
        { new int[][] {
                {1, 4, 2, 3},
                {2, 3, 4, 1},
                {4, 1, 0, 2},
                {3, 2, 1, 4}
            }                                   ,  false },//13 FAIL
        { new int[][] {
                {1, 4, 2, 3},
                {2, 3, 4, 1},
                {4, 4, 3, 2},
                {3, 2, 1, 4}
            }                                   ,  false },//14
        { new int[][]  {
                {1, 4, 2, 3},
                {2, 3, 4, 1},
                {4, 1, 3, 2},
                {3, 2, 1, 4}
            }                                   ,  true  },//15
        { new int[][] {
                {1, 5, 2, 3},
                {2, 3, 4, 1},
                {4, 1, 3, 2},
                {3, 2, 1, 5}
            }                                   ,  false },//16
        { new int[][] {
                {1, 4, 2, 0},
                {2, 3, 4, 1},
                {4, 1, 0, 2},
                {3, 2, 1, 4}
            }                                   ,  false },//17 FAIL
        { new int[][] {
                {1, 4, 2, 2},
                {2, 3, 4, 1},
                {4, 4, 3, 2},
                {3, 2, 1, 4}
            }                                   ,  false },//18
        { new int[][] {
                {1, 5, 4, 8, 7, 3, 2, 9, 6},
                {3, 8, 6, 5, 9, 2, 7, 1 ,4},
                {7, 2, 9, 6, 4, 1, 8, 3, 5},
                {8, 6, 3, 7, 2, 5, 1, 4, 9},
                {9, 7, 5, 3, 1, 4, 6, 2, 8},
                {4, 1, 2, 9, 6, 8, 3, 5, 7},
                {6, 3, 1, 4, 5, 7, 9, 8, 2},
                {5, 9, 8, 2, 3, 6, 4, 7, 1},
                {2, 4, 7, 1, 8, 9, 5, 6, 3}
            }                                  ,  true  },//19

        /*  Yifan Zhou test yifan.zhou1@students.mq.edu.au */
        { new int[][] {
                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {2, 3, 4, 5, 6, 7, 8, 9, 1},
                {3, 4, 5, 6, 7, 8, 9, 1, 2},
                {4, 5, 6, 7, 8, 9, 1, 2, 3},
                {5, 6, 7, 8, 9, 1, 2, 3, 4},
                {6, 7, 8, 9, 1, 2, 3, 4, 5},
                {7, 8, 9, 1, 2, 3, 4, 5, 6},
                {8, 9, 1, 2, 3, 4, 5, 6, 7},
                {9, 1, 2, 3, 4, 5, 6, 7, 8}

            }                               ,   false  }//20
    });
  }
  /* *INDENT-ON* */

  // Test parameters
  // @link{https://github.com/junit-team/junit4/wiki/Parameterized-tests}
  private int[][] grid;
  private boolean validStatus;

  /**
   *  Constructor for the tests
   */
  public ValidSudokuParameterisedTests(int[][] input, boolean expectedValidStatus) {
    grid = input;
    validStatus = expectedValidStatus;
  }

  /**
   * Run all the tests
   */
  @Test
  public void test()  {
    assertEquals(validStatus, new Sudoku(grid).isValid());
  }
}
